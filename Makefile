.PHONY: all xnnpack armcpu mali

all: xnnpack armcpu mali

init:
	git submodule update --init --recursive

xnnpack: init
	cd ACLTuner-XNNPack && \
	make build && \
	mv Output ../XNNPack

armcpu: init
	cd ACLTuner-Neon && \
	make build && \
	mv Output ../Neon

mali: init
	cd ACLTuner-Mali && \
	make build && \
	mv Output ../Mali
